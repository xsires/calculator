package ru.xsires.calculator;

import static org.junit.Assert.assertEquals;

import java.util.Random;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.junit.Before;
import org.junit.Test;

import ru.xsires.calculator.tree.BinaryOperations;
import ru.xsires.calculator.tree.UnaryOperations;

/**
 * Глубокий тест калькулятора, самодельная кривая generateEquation генерирует уравнения, решаются Nathorn'ом и сверяются
 */
public class CalculatorDeepTest {

	static final int MAX = 10_000;

	Random rnd = new Random(System.currentTimeMillis());

	boolean previousNumber(StringBuilder sb) {
		return sb.length() == 0 || Character.isDigit(sb.charAt(sb.length() - 1));
	}

	boolean previousOpenBreacket(StringBuilder sb) {
		return sb.length() == 0 || (sb.charAt(sb.length() - 1)) == '(';
	}

	boolean previousBinOperation(StringBuilder sb) {
		String string = sb.toString();
		boolean result = false;
		for (BinaryOperations bi : BinaryOperations.values()) {
			result |= string.endsWith(bi.getCharacter());
		}
		return result;
	}

	String generateEquation() {
		StringBuilder result = new StringBuilder();
		int breaketDeep = 0;
		for (int i = 0; i < 20; i++) {
			switch (rnd.nextInt(4)) {
			case 0: // number
				if (result.length() == 0 || previousOpenBreacket(result)) {
					result.append(String.format("%.5f", rnd.nextInt(1000) * (rnd.nextBoolean() ? rnd.nextFloat() : 1)));
				}
				break;
			case 1: // ( )
				if (breaketDeep == 0 || rnd.nextBoolean()) {
					if (!previousNumber(result)) {
						result.append("(");
						breaketDeep++;
					}

				} else {
					if (!previousOpenBreacket(result)) {
						result.append(")");
						breaketDeep--;
					}
				}
			case 2: // binar
				if (result.length() != 0 && previousNumber(result)) {
					result.append(BinaryOperations.values()[rnd.nextInt(BinaryOperations.values().length)]
							.getCharacter());
				}
			case 4: // unar
				if (result.length() == 0 || previousBinOperation(result)) {
					String character = UnaryOperations.values()[rnd.nextInt(UnaryOperations.values().length)]
							.getCharacter();
					if (character == "~") {
						character = "-";
					}
					result.append(character).append("(");
					breaketDeep++;
				}
			}
		}
		if (previousOpenBreacket(result)) {
			result.append(String.format("%.5f", rnd.nextInt() * (rnd.nextBoolean() ? rnd.nextFloat() : 1)));
		}
		while (breaketDeep != 0) {
			result.append(')');
			breaketDeep--;
		}
		String equation = result.toString();
		while (equation.contains("--") || equation.contains("++") || equation.contains("-+") || equation.contains("+-")) {
			equation = equation.replaceAll("\\-\\-", "+").replaceAll("\\+\\+", "+").replaceAll("\\-\\+", "-")
					.replaceAll("\\+\\-", "-");
		}
		return equation.replaceAll("\\)\\(", ")+(").replaceAll("\\,", ".");
	}

	ScriptEngine nashornEngine;

	@Before
	public void beforeTest() {
		nashornEngine = new ScriptEngineManager().getEngineByName("nashorn");
	}

	@Test
	public void deep() throws ScriptException {
		int prevPrint = 0;
		for (int i = 0; i < MAX; i++) {
			String generateEquation = generateEquation();
			// т.к. в js sin(3) -> Math.sin(3)
			String jsPrepare = generateEquation.replaceAll("sin", "Math.sin").replaceAll("cos", "Math.cos")
					.replaceAll("exp", "Math.exp");
			Double nashornSolv = (Double) nashornEngine.eval(jsPrepare);
			// System.out.println(generateEquation + "=" + nashornSolv);
			assertEquals(nashornSolv, Calculator.create(generateEquation).evalueate(), 0.1);

			int pc = (i / (MAX / 100));
			if (pc % 5 == 0 && pc != prevPrint) {
				System.out.println("Solve: " + pc + "%" + " count " + i);
				prevPrint = pc;
			}
		}
		System.out.println("Solve: 100%" + " count " + MAX);
	}

}
