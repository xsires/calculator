package ru.xsires.calculator;

import static org.junit.Assert.assertEquals;

import org.junit.Ignore;
import org.junit.Test;

public class CalculatorTest {

	@Test
	public void prepare() {
		assertEquals("2+4.2+3.3", Calculator.prepare("2   + 4,2 + 3.3"));
		assertEquals("~2+3", Calculator.prepare("-2+3"));
		assertEquals("3-(~2+3)", Calculator.prepare("3-(-2+3)"));
		assertEquals("3-(~4)", Calculator.prepare("3-(-4)"));
		assertEquals("2+3", Calculator.prepare("2--3"));
		assertEquals("2-3", Calculator.prepare("2---3"));
		assertEquals("2-3+3-3-3+3", Calculator.prepare("2---3++3-+3+-3++3"));
		assertEquals("2*~(3+4)", Calculator.prepare("2*-(3+4)"));
		assertEquals("sin(3)/~(6+3)", Calculator.prepare("sin(3)/-(6+3)"));
		assertEquals("3-4+368.00000", Calculator.prepare("3-4+368.00000"));

	}

	@Test
	public void evalueate() {
		assertEquals(111, Calculator.create("11 + (exp(2.010635 + sin(PI/2)*3) + 50) / 2").evalueate(), 0.1); // 110.99997794278411
		assertEquals(-1, Calculator.create("cos(-pi)").evalueate(), 1);
		assertEquals(1, Calculator.create("sin(pi/2)").evalueate(), 0);
		assertEquals(4, Calculator.create("2*(2)").evalueate(), 0);
		assertEquals(72, Calculator.create("2*(2*(2*(2+2)+(2+2)*2+2))").evalueate(), 0);
		assertEquals(5, Calculator.create("2+3").evalueate(), 0);
		assertEquals(4, Calculator.create("-(1-2)+3").evalueate(), 0);
		assertEquals(2, Calculator.create("-1-(-1-2)").evalueate(), 0);
		assertEquals(Double.NaN, Calculator.create("0/0").evalueate(), 0);
		assertEquals(-0.4338837391175581, Calculator.create("sin(pi/-(3+4))").evalueate(), 0);
		assertEquals(3 - 4 + 5, Calculator.create("3-4+5").evalueate(), 0);
		assertEquals(-0.5110280402362375, Calculator
				.create("cos((71.36704-(457.00000)+(((368.00000/cos(48.23604))))))").evalueate(), 0);

	}

	@Test(expected = NumberFormatException.class)
	public void fail1() {
		assertEquals(1d, Calculator.create("sin1(2)").evalueate(), 0);
	}

	@Test(expected = StackOverflowError.class)
	@Ignore
	public void fail2() {
		assertEquals(1d, Calculator.create(StringEquationUtils.repeat("1*", 100500)).evalueate(), 0);
	}

}
