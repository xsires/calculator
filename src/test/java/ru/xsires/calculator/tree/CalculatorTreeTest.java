package ru.xsires.calculator.tree;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CalculatorTreeTest {

	@Test
	public void lowPriorityOperation() {
		CalculatorTree n = new CalculatorTree();
		assertEquals(5, n.getLowPriorityOperation("1+3*4+4"));
		assertEquals(5, n.getLowPriorityOperation("1*3*4*4"));
		assertEquals(5, n.getLowPriorityOperation("1+3-4+4"));
		assertEquals(3, n.getLowPriorityOperation("1+3+(4*4)*2"));
		assertEquals(23, n.getLowPriorityOperation("1+3+(4*(3+4)+5+(3*3)+4)+2"));
		assertEquals(5, n.getLowPriorityOperation("1*3*4+4"));
	}

	@Test
	public void getDiget() {
		CalculatorTree n = new CalculatorTree();
		assertEquals(134d, n.getDiget("134"), 0);
		assertEquals(1.3d, n.getDiget("1.3"), 0);
		assertEquals(Math.PI, n.getDiget("pi"), 0);
		assertEquals(Math.E, n.getDiget("e"), 0);

	}

	@Test(expected = NumberFormatException.class)
	public void fail() {
		assertEquals(null, new CalculatorTree().getDiget("1=P.3"));
	}

}
