package ru.xsires.calculator;

public class StringEquationUtils {
	/**
	 * 
	 * @param equation
	 * @return (((3)+(4))) - > (3)+(4)
	 */
	public static String openBracketsIfNecessary(String equation) {
		while (!equation.isEmpty() && equation.charAt(0) == '('
				&& getClosingBracket(equation, 0) == equation.length() - 1) {
			equation = equation.substring(1, equation.length() - 1);
		}
		return equation;
	}

	/**
	 * @param sb
	 * @param openBracket
	 *            - позиция открытой скобки
	 * @return - позиция закрытой скобки
	 */
	public static int getClosingBracket(String sb, int openBracket) {
		int deep = 0;
		for (int i = openBracket; i < sb.length(); i++) {
			char ch = sb.charAt(i);
			if (ch == '(') {
				deep++;
			}
			if (ch == ')') {
				deep--;
			}
			if (deep == 0) {
				return i;
			}
		}
		throw new RuntimeException("Invalid " + sb.toString());
	}

	/**
	 * Не хотелось тащить Apache Commons Lang
	 */
	public static String repeat(String st, int times) {
		StringBuilder result = new StringBuilder(st.length() * times);
		for (int i = 0; i < times; i++) {
			result.append(st);
		}
		return result.toString();
	}
}
