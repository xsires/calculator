package ru.xsires.calculator;

import ru.xsires.calculator.tree.CalculatorTree;

public class Calculator extends CalculatorTree {

	protected Calculator(String equation) {
		super(equation);
	}

	/**
	 * Чистим введенную строку от всего лишнего
	 * 
	 * @param equation
	 * @return
	 */
	static String prepare(String equation) {
		equation = equation.toLowerCase().replaceAll("\\s", "").replace(",", ".");

		while (equation.contains("--") || equation.contains("++") || equation.contains("-+") || equation.contains("+-")) {
			equation = equation.replaceAll("\\-\\-", "+").replaceAll("\\+\\+", "+").replaceAll("\\-\\+", "-")
					.replaceAll("\\+\\-", "-");
		}

		// ../-(..) -> ../~(..)
		// ..*-(..) -> ..*~(..)
		equation = equation.replaceAll("\\/\\-\\(", "/~(").replaceAll("\\*\\-\\(", "*~(");

		equation = equation.replaceAll("\\/\\-", "/~").replaceAll("\\*\\-", "*~");

		if (equation.startsWith("-")) {
			equation = new StringBuilder(equation).deleteCharAt(0).insert(0, "~").toString();
		}

		equation = equation.replaceAll("\\(\\-", "(~");

		return equation;
	}

	public static CalculatorTree create(String equation) {
		return new Calculator(prepare(equation));
	}
}
