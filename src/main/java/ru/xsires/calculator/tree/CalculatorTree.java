package ru.xsires.calculator.tree;

import static ru.xsires.calculator.StringEquationUtils.openBracketsIfNecessary;
import static ru.xsires.calculator.tree.BinaryOperations.getBinaryOperation;
import static ru.xsires.calculator.tree.UnaryOperations.escapeEquation;
import static ru.xsires.calculator.tree.UnaryOperations.getUnaryOperation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ru.xsires.calculator.StringEquationUtils;

/**
 * Узел дерева для решения
 * 
 */
public class CalculatorTree {

	private String nodeSource;
	private CalculatorTree left;
	private CalculatorTree right;
	private Double value;
	private BinaryOperations binaryOperations;
	private List<UnaryOperations> unaryOperations = new ArrayList<UnaryOperations>();

	protected CalculatorTree() {
	}

	/**
	 * Use {@link ru.xsires.calculator.Calculator}
	 * 
	 * @param equation
	 */
	protected CalculatorTree(String equation) {
		nodeSource = equation;
		// первым делом избавимся от скобок и унарных операций
		UnaryOperations unaryOperation = null;
		equation = openBracketsIfNecessary(equation);
		while ((unaryOperation = getUnaryOperation(equation)) != null) {
			unaryOperations.add(unaryOperation);
			equation = escapeEquation(equation, unaryOperation);
		}
		equation = openBracketsIfNecessary(equation);
		// поищем бинарную операцию с самым низким приоритетом
		int priorityOperation = getLowPriorityOperation(equation);
		if (priorityOperation != -1) {
			// если нашли входим в рекурсию
			this.binaryOperations = getBinaryOperation(equation, priorityOperation);
			this.left = new CalculatorTree(equation.substring(0, priorityOperation));
			this.right = new CalculatorTree(equation.substring(priorityOperation + 1, equation.length()));
		} else {
			// если не нашли то это цифра
			this.value = getDiget(equation.toString());
		}
	}

	/**
	 * @return результат примера
	 */
	public double evalueate() {
		double nodeResult;
		if (binaryOperations != null) {
			nodeResult = binaryOperations.execute(left.evalueate(), right.evalueate());
		} else {
			nodeResult = value;
		}
		// т.к. sin(-pi) != -sin(pi)
		// unaryOperations это стэк, а должна быть очередь, ведь порядок выполнения унарных операций важен
		Collections.reverse(unaryOperations);
		for (UnaryOperations uo : unaryOperations) {
			nodeResult = uo.execute(nodeResult);
		}
		return nodeResult;
	}

	protected Double getDiget(String eq) {
		if ("pi".equals(eq)) {
			return Math.PI;
		}
		if ("e".equals(eq)) {
			return Math.E;
		}
		return Double.parseDouble(eq);

	}

	/**
	 * позиция наимение приоритетной операции или -1 <br>
	 * (3+4)*3*5 - > 5<br>
	 * 3-4+5 -> 3
	 * 
	 * @param sb
	 * @return
	 */
	protected int getLowPriorityOperation(String sb) {
		int low = -1;
		int high = -1;
		int index = 0;
		while (index < sb.length()) {
			char ch = sb.charAt(index);
			if (BinaryOperations.isLowPriority(String.valueOf(ch))) {
				low = index;
			} else if (BinaryOperations.isHighPriority(String.valueOf(ch))) {
				high = index;
			} else if (ch == '(') {
				index = StringEquationUtils.getClosingBracket(sb, index);
			}
			index++;
		}
		return low == -1 ? high : low;
	}

	private void print(int deep) {
		System.out.print(StringEquationUtils.repeat("\t", deep));

		for (UnaryOperations ou : unaryOperations) {
			System.out.print(" " + ou.getCharacter());
		}
		if (value != null) {
			System.out.print(" " + value);
		}
		if (binaryOperations != null) {
			System.out.print(" " + binaryOperations.getCharacter());
		}
		System.out.println(" " + nodeSource + " ->");
		if (left != null) {
			left.print(deep + 1);
		}
		if (right != null) {
			right.print(deep + 1);
		}
	}

	/**
	 * рисуем дерево вычислений в System.out
	 */
	public void printTree() {
		print(0);
	}

	@Override
	public String toString() {
		return "CalculatorTree [nodeSource=" + nodeSource + ", value=" + value + ", binaryOperations="
				+ binaryOperations + ", unaryOperations=" + unaryOperations + "]";
	}

}
