package ru.xsires.calculator.tree;

import ru.xsires.calculator.StringEquationUtils;

/**
 * Операции выполняемые над одним предикатом, sin cos exp, отрицание тоже здесь
 */
public enum UnaryOperations {

	SIN((double e) -> {
		return (Math.sin(e));
	}, "sin"),

	COS((double e) -> {
		return (Math.cos(e));
	}, "cos"),

	EXP((double e) -> {
		return (Math.exp(e));
	}, "exp"),

	MINUS((double e) -> {
		return (-e);
	}, "~");

	private UnaryOperation operation;

	private String character;

	UnaryOperations(UnaryOperation operation, String character) {
		this.operation = operation;
		this.character = character;
	}

	/**
	 * sin(3+5) - > sin <br>
	 * sin(3)+4 - > null <br>
	 * -3 -> ~ <br>
	 * -2+sin3 -> null
	 * 
	 * @return
	 */
	public static UnaryOperations getUnaryOperation(String eq) {
		UnaryOperations result = null;
		for (UnaryOperations uo : UnaryOperations.values()) {
			if (eq.startsWith(uo.getCharacter())) {
				result = uo;
				break;
			}
		}
		if (result != null) {
			if (eq.charAt(result.getCharacter().length()) == '(') {
				int closingBracket = StringEquationUtils.getClosingBracket(eq, result.getCharacter().length());
				if (closingBracket != eq.length() - 1) {
					result = null;
				}
			} else {
				int index = 0;
				while (index < eq.length()) {
					char ch = eq.charAt(index);
					if (BinaryOperations.isBinaryOperation(String.valueOf(ch))) {
						return null;
					} else if (ch == '(') {
						index = StringEquationUtils.getClosingBracket(eq, index);
					}
					index++;
				}
			}
		}
		return result;
	}

	/**
	 * sin(3+44) -> 3+44 <br/>
	 * sin4 -> 4
	 * 
	 * 
	 * @param sb
	 * @param uo
	 * @return
	 */
	public static String escapeEquation(String sb, UnaryOperations uo) {
		StringBuilder stringBuilder = new StringBuilder(sb);
		stringBuilder.delete(0, uo.getCharacter().length());
		return StringEquationUtils.openBracketsIfNecessary(stringBuilder.toString());
	}

	public String getCharacter() {
		return character;
	}

	public double execute(double element) {
		return operation.execute(element);
	}

}

interface UnaryOperation {
	double execute(double element);
}