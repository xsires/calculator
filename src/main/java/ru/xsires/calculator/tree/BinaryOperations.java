package ru.xsires.calculator.tree;

/**
 * Операции которые выполняются над двумя предикатами сложение, умножение, деление и т.д.
 */
public enum BinaryOperations {

	MINUS((double left, double rigtht) -> {
		return left - rigtht;
	}, "-"),

	PLUS((double left, double rigtht) -> {
		return left + rigtht;
	}, "+"),

	MULT((double left, double rigtht) -> {
		return left * rigtht;
	}, "*"),

	DIV((double left, double rigtht) -> {
		return left / rigtht;
	}, "/");

	private BinaryOperation operation;
	private String character;

	BinaryOperations(BinaryOperation operation, String character) {
		this.operation = operation;
		this.character = character;
	}

	/**
	 * По хорошему у бинарных операций должно быть свойство "приоритет" но мы разделяем приоритет на "высокий" и
	 * "низкий" тут нет смысле оверхэдить.
	 * 
	 * @param operation
	 * @return
	 */
	public static boolean isHighPriority(String operation) {
		return operation.equals(DIV.getCharacter()) || operation.equals(MULT.getCharacter());
	}

	public static boolean isLowPriority(String operation) {
		return operation.equals(MINUS.getCharacter()) || operation.equals(PLUS.getCharacter());
	}

	public static BinaryOperations getBinaryOperation(String sb, int pos) {
		String substring = sb.substring(pos);
		for (BinaryOperations bo : BinaryOperations.values()) {
			if (substring.startsWith(bo.getCharacter())) {
				return bo;
			}
		}
		return null;
	}

	public static boolean isBinaryOperation(String operation) {
		for (BinaryOperations bo : BinaryOperations.values()) {
			if (bo.getCharacter().equals(operation)) {
				return true;
			}
		}
		return false;
	}

	public String getCharacter() {
		return character;
	}

	public double execute(double left, double rigtht) {
		return operation.execute(left, rigtht);
	}
}

interface BinaryOperation {
	double execute(double left, double rigtht);
}
