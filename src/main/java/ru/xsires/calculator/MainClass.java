package ru.xsires.calculator;

public class MainClass {

	public static void main(String[] args) {
		System.out.print("Enter example: ");
		String example = System.console().readLine();
		try {
			double evalueate = Calculator.create(example).evalueate();
			System.out.format("%.5f%n", evalueate);
		} catch (NumberFormatException e) {
			System.err.println("Error:" + e.getMessage());
		}
	}
}
